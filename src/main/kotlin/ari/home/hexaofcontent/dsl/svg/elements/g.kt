package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.core.Attribute
import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.SvgElement

class G(parent: SvgElement? = null): SvgElement("g", parent=parent) {
    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    override fun geometry(): Geometry {
        return this.children.filterIsInstance(SvgElement::class.java).map { it.geometry() }.fold(super.geometry()) { a, b -> a + b }
    }
}


fun SvgElement.g(init: G.() -> Unit): SvgElement {
    val g = G(parent=this)
    g.init()
    this.children.add(g)
    return g
}
