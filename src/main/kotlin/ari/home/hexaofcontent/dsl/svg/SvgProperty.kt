package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.core.Attribute

class SvgProperty(name: String, var value: Any?): Attribute(name) {
    override fun render(): String {
        return "$name=\"${value()}\""
    }

    override fun value(): String {
        return value.toString().replace("\\.0$".toRegex(), "")
    }

    override fun valid(): Boolean {
        return value != null && value.toString() != ""
    }

    operator fun invoke(value: Any): SvgProperty {
        this.value = value
        return this
    }

    infix fun `is`(value: String): Boolean {
        return this.value.toString() == value
    }
}
