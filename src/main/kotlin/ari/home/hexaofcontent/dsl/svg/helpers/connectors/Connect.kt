package ari.home.hexaofcontent.dsl.svg.helpers.connectors

import ari.home.hexaofcontent.dsl.core.Anchor
import ari.home.hexaofcontent.dsl.svg.SvgElement

fun SvgElement.connect(source: Anchor, destination: Anchor, type: ConnectorType = ConnectorType.Straight, ending: Marker = Marker.None, starting: Marker = Marker.None, endingSize: Double = 10.0, init: Connector.() -> Unit = {}): Connector {
    val line = when (type) {
        ConnectorType.Squared ->  SquaredConnector(source, destination, endingSize)
        ConnectorType.Curvy ->  CurvyConnector(source, destination, endingSize)
        else -> StraightConnector(source, destination, endingSize)
    }
    line.applyEndings(starting, ending)
    line.init()

    this.children.add(line)
    return line
}
