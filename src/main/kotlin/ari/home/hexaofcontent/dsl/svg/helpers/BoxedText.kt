package ari.home.hexaofcontent.dsl.svg.helpers

import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.SvgElement
import ari.home.hexaofcontent.dsl.svg.elements.rect
import ari.home.hexaofcontent.dsl.svg.elements.text as svgText


class BoxedText(parent: SvgElement? = null): SvgElement("g", parent) {

    val rect: SvgElement = rect {
        "fill"("none")
        "stroke"("black")
    }
    val content = svgText {}

    fun box(init: SvgElement.() -> Unit) {
        rect.init()
    }

    override fun geometry(): Geometry {
        return rect.geometry()
    }

    override fun geometry(init: Geometry.() -> Unit) {

        rect.geometry(init)
        val center = rect.geometry().center()

        content.geometry {
            x(center.x)
            y(center.y)
        }
    }

    fun text(value: String, init: SvgElement.() -> Unit = {}) {
        val basicStyle: SvgElement.() -> Unit = {
            - value
            "text-anchor"("middle")
            "dominant-baseline"("middle")
        }
        this.content.basicStyle()
        this.content.init()
    }
}

fun boxedText(init: BoxedText.() -> Unit): SvgElement {
    val element = BoxedText()
    element.init()
    return element
}

fun SvgElement.boxedText(init: BoxedText.() -> Unit): SvgElement {
    val element = BoxedText(this)
    element.init()
    this.children.add(element)
    return this
}
