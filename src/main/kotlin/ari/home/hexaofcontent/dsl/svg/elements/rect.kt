package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.svg.SvgElement

class Rect(parent: SvgElement? = null): SvgElement("rect", parent=parent) {
    fun x(value: Number) {
        geometry { x(value) }
    }

    fun y(value: Number) {
        geometry { y(value) }
    }

    fun width(value: Number) {
        geometry { width(value) }
    }

    fun height(value: Number) {
        geometry { height(value) }
    }
}

private fun create(init: Rect.() -> Unit, parent: SvgElement?): Rect {
    val element = Rect(parent)
    element.init()
    return element
}

fun SvgElement.rect(init: Rect.() -> Unit): Rect {
    val element = create(init, this)
    this.children.add(element)
    return element
}
