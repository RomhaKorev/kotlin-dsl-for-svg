package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.core.Attribute
import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.SvgElement


class Circle(parent: SvgElement? = null): SvgElement("circle", parent=parent) {
    private var cx: Double = 0.0
    private var cy: Double = 0.0

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    fun cx(value: Number) {
        geometry { x(value) }
        "cx"(value)
        cx = value.toDouble()
    }

    fun cy(value: Number) {
        geometry { y(value) }
        "cy"(value)
        cy = value.toDouble()
    }

    fun r(v: Number) {
        val value = v.toDouble()
        geometry {
            width(value * 2)
            height(value * 2)
            x(cx - value)
            y(cy - value)
        }
        "r"(value)
    }
}

private fun create(init: Circle.() -> Unit, parent: SvgElement?): Circle {
    val element = Circle(parent)
    element.init()
    return element
}

fun SvgElement.circle(init: Circle.() -> Unit): Circle {
    val element = create(init, this)
    this.children.add(element)
    return element
}
