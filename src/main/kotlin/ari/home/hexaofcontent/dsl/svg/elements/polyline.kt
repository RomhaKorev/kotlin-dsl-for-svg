package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.core.Attribute
import ari.home.hexaofcontent.dsl.core.Point
import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.SvgElement
import kotlin.math.abs

class Polyline(parent: SvgElement? = null): SvgElement("polyline", parent=parent) {
    var points = listOf<Point>()

    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    fun points(vararg points: Point) {
        this.points = points.toList()

        "points"(this.points.joinToString(", ") {(x, y) -> "$x $y" }.replace("(\\d+)\\.0+(\\D)".toRegex(), "$1$2"))

        val x = points.map { it.x }.minOrNull() ?: return
        val y = points.map { it.y }.minOrNull() ?: return
        val width = abs((points.map { it.x }.maxOrNull() ?: return) - x)
        val height = abs((points.map { it.y }.maxOrNull() ?: return) - y)

        geometry {
            x(x)
            y(y)
            width(width)
            height(height)
        }
    }
}


fun SvgElement.polyline(init: Polyline.() -> Unit): Polyline {
    val element = Polyline(this)
    element.init()
    this.children.add(element)
    return element
}
