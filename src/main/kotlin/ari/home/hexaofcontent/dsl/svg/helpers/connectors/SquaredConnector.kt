package ari.home.hexaofcontent.dsl.svg.helpers.connectors

import ari.home.hexaofcontent.dsl.core.Anchor
import ari.home.hexaofcontent.dsl.core.Point
import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.SvgElement
import ari.home.hexaofcontent.dsl.svg.elements.Line
import ari.home.hexaofcontent.dsl.svg.elements.g
import ari.home.hexaofcontent.dsl.svg.elements.line
import ari.home.hexaofcontent.dsl.svg.orphan

class SquaredConnector(source: Anchor, destination: Anchor, endingSize: Double = 10.0): Connector(source, destination, endingSize) {


    private val points = createPath(source, destination)
    private val lines: List<Line> = points.zipWithNext().mapIndexed { index, (a, b) ->
        orphan {
            line {
                if (!(index == 0 || index == points.lastIndex))
                    "stroke-linecap"("round")
                x1(a.x)
                y1(a.y)
                x2(b.x)
                y2(b.y)
            }
        }
    }
    init {
        connector = g {
            this.children.addAll(lines)
        }
    }

    override fun geometry(): Geometry {
        return connector.geometry()
    }

    override fun applyArrowAtStart(identifier: Int) {
        val segment = lines.first().toSegment()
        val newEnd = segment.pointAtPointFromStart(20.0)

        val applyArrow : Line.() -> Unit = {
            x1(newEnd.x)
            y1(newEnd.y)
            "marker-start"("url(#startarrow-$identifier)")
        }

        lines.first().applyArrow()
    }

    override fun applyArrowAtEnd(identifier: Int) {
        val segment = lines.last().toSegment()//Segment(source.point, destination.point)
        val newEnd = segment.pointAtPointFromEnd(20.0)

        val applyArrow : SvgElement.() -> Unit = {
            "x2"(newEnd.x)
            "y2"(newEnd.y)
            "marker-end"("url(#endarrow-$identifier)")
        }

        lines.last().applyArrow()
    }

    companion object {
        fun createPath(source: Anchor, destination: Anchor): List<Point> {
            val startHorizontally = (source.kind `is` (Anchor.Kind.Right or Anchor.Kind.Left))
            val endHorizontally = (destination.kind `is` (Anchor.Kind.Right or Anchor.Kind.Left))

            return if (startHorizontally == endHorizontally && startHorizontally) {
                val x = source.x + (destination.x - source.x)/2.0
                listOf(source.point, Point(x, source.y), Point(x, destination.y), destination.point)
            } else if (startHorizontally == endHorizontally && !startHorizontally) {
                val y = source.y + (destination.y - source.y)/2.0
                listOf(source.point, Point(source.x, y), Point(destination.x, y), destination.point)
            } else if (startHorizontally != endHorizontally && startHorizontally) {
                listOf(source.point, Point(destination.x, source.y), destination.point)
            } else {
                listOf(source.point, Point(source.x, destination.y), destination.point)
            }
        }
    }
}
