package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.core.Anchor
import ari.home.hexaofcontent.dsl.core.Attribute
import ari.home.hexaofcontent.dsl.core.Tag
import ari.home.hexaofcontent.dsl.svg.elements.*
import ari.home.hexaofcontent.dsl.svg.elements.line

open class SvgElement(name: String, val parent: SvgElement? = null): Tag(name) {
    val id = svgProperty("id", "")
    var transparent: Boolean = false

    fun transparent() {
        transparent = true
    }

    operator fun String.invoke(init: SvgElement.() -> Unit): SvgElement {
        fun SvgElement.andWarn(name: String, tag: String=name): SvgElement {
            println("A $name should be built by using $tag {...} instead of \"$tag\"{...}")
            return this
        }
        when(this) {
            "g" -> return g(init).andWarn("group", "g")
            "line" -> return line(init, generateGeometry = false).andWarn("line")
            "rect" -> return rect(init).andWarn("rectangle", "rect")
            "circle" -> return circle(init).andWarn("circle")
            "polyline" -> return polyline(init).andWarn("polyline")
            "text" -> return text(init).andWarn("text")
        }
        val instance = element(this, this@SvgElement, init)
        children.add(instance)
        return instance
    }

    private fun svgProperty(name: String, value: String): SvgProperty {
        val property = SvgProperty(name, value)
        this.attributes.add(property)
        return property
    }

    fun findSibling(sourceId: String): SvgElement? {
        return if (this.parent == null) {
            val all = flatten()
            all.find { it.id `is` sourceId }
        } else {
            this.parent.findSibling(sourceId)
        }
    }

    fun flatten(): List<SvgElement> {
        return this.children.filterIsInstance<SvgElement>().flatMap { it.flatten() } + this
    }

    open fun boundingRect(): Geometry {
        val childrenGeometries = children.filterIsInstance<SvgElement>().filter { !it.transparent }.map { it.geometry() }
        return childrenGeometries.fold(this.geometry()) { acc, elementGeometry -> acc + elementGeometry }
    }

    private val geometry = Geometry()
    open fun geometry(init: Geometry.() -> Unit) {
        geometry.init()
    }

    open fun geometry(): Geometry {
        return geometry
    }

    fun String.top(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().top()
    }

    fun String.bottom(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().bottom()
    }

    fun String.right(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().right()
    }

    fun String.left(): Anchor {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry().left()
    }

    fun String.valueOf(attribute: String): String {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.attribute(attribute).value()
    }

    fun String.geometry(): Geometry {
        val source = findSibling(this) ?: throw IllegalArgumentException("$this does not exist")
        return source.geometry()
    }

    fun sameSizeThan(id: String) {
        val otherGeometry = id.geometry()
        geometry {
            width(otherGeometry.width())
            height(otherGeometry.height())
        }
    }

    fun Number.below(id: String) {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val offset = this.toDouble()
        geometry {
            y(offset + siblingGeometry.bottom().y)
        }
    }

    fun Number.above(id: String) {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val offset = this.toDouble()
        geometry {
            y(siblingGeometry.top().y - geometry().height() - offset)
        }
    }

    fun Number.leftOf(id: String) {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val offset = this.toDouble()
        geometry {
            x(siblingGeometry.left().x - geometry().width() - offset)
        }
    }

    fun Number.rightOf(id: String) {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()
        val asDouble = this.toDouble()
        geometry {
            x(asDouble + siblingGeometry.right().x)
        }
    }

    fun alignRight(id: String) {
        val sibling = findSibling(id) ?: throw IllegalArgumentException("$id does not exist")
        val siblingGeometry = sibling.geometry()

        geometry {
            x(siblingGeometry.right().x - geometry().width())
        }
    }

    override fun args(): String {
        return listOf(geometry.toAttributes().joinToString(" ") { it.render() }, super.args()).filter { it.isNotBlank() }.joinToString(" ")
    }

    protected open fun Geometry.toAttributes(): List<Attribute> {
        return listOf(SvgProperty("x", xOrNull()),
            SvgProperty("y", yOrNull()),
            SvgProperty("width", widthOrNull()),
            SvgProperty("height", heightOrNull())
        ).filter { it.valid() }
    }
}
