package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.core.Anchor
import ari.home.hexaofcontent.dsl.core.Point
import kotlin.math.max
import kotlin.math.min


class Geometry(private var x: Double? = null,
    private var y: Double? = null,
    private var width: Double? = null,
    private var height: Double? = null,
    private var valid: Boolean = x != null || y != null || width != null || height != null) {

    operator fun component1(): Double = x()
    operator fun component2(): Double = y()
    operator fun component3(): Double = width()
    operator fun component4(): Double = height()

    fun valid(): Boolean {
        return valid
    }

    fun x(value: Number) {
        x = value.toDouble()
        valid = true
    }

    fun xOrNull(): Double? = x
    fun yOrNull(): Double? = y
    fun widthOrNull(): Double? = width
    fun heightOrNull(): Double? = height

    fun y(value: Number) {
        y = value.toDouble()
        valid = true
    }

    fun width(value: Number) {
        width = value.toDouble()
        valid = true
    }

    fun height(value: Number) {
        height = value.toDouble()
        valid = true
    }

    fun center(): Point {
        return Point(
            this.x() + width() / 2.0,
            this.y() + height() / 2.0
        )
    }

    fun x(): Double {
        return x ?: 0.0
    }

    fun y(): Double {
        return y ?: 0.0
    }

    fun width(): Double {
        return width ?: 0.0
    }

    fun height(): Double {
        return height ?: 0.0
    }

    fun top(): Anchor {
        return Anchor(Point(center().x, y ?: 0.0), Anchor.Kind.Top)
    }

    fun bottom(): Anchor {
        return Anchor(Point(center().x, y() + height()), Anchor.Kind.Bottom)
    }

    fun left(): Anchor {
        return Anchor(Point(x(), center().y), Anchor.Kind.Left)
    }

    fun right(): Anchor {
        return Anchor(Point(x() + width(), center().y), Anchor.Kind.Right)
    }

    operator fun plus(other: Geometry): Geometry {
        if (!other.valid)
            return this
        if (!valid)
            return other

        val farLeft = min(this.left().x, other.left().x)
        val farTop = min(this.top().y, other.top().y)
        val farRight = max(this.right().x, other.right().x)
        val farBottom = max(this.bottom().y, other.bottom().y)

        return Geometry( farLeft, farTop, Math.abs(farRight - farLeft), Math.abs(farTop - farBottom), true )
    }

    operator fun plus(other: Offset): Geometry {
        return Geometry(
            x() + other.dx.toDouble(),
            y() + other.dy.toDouble(),
            width() + other.dw.toDouble(),
            height() + other.dh.toDouble()
        )
    }

    operator fun plus(translation: Point): Geometry {
        return this + Offset(translation.x, translation.y, 0, 0)
    }

    override fun equals(other: Any?): Boolean {
        return other is Geometry
                && x == other.x
                && y == other.y
                && width == other.width
                && height == other.height
    }
}

data class Offset(val dx: Number, val dy: Number, val dw: Number, val dh: Number)
