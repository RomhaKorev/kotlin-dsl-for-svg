package ari.home.hexaofcontent.dsl.svg.helpers.connectors

import ari.home.hexaofcontent.dsl.core.*


enum class ConnectorType {
    Straight,
    Squared,
    Curvy
}





