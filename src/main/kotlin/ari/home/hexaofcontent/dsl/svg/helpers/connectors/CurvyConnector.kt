package ari.home.hexaofcontent.dsl.svg.helpers.connectors

import ari.home.hexaofcontent.dsl.core.Anchor
import ari.home.hexaofcontent.dsl.core.Point
import ari.home.hexaofcontent.dsl.svg.SvgElement
import ari.home.hexaofcontent.dsl.svg.orphan
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class CurvyConnector(source: Anchor, destination: Anchor, endingSize: Double = 10.0): Connector(source, destination, endingSize) {
    private val controlPoints = createControlPoints(source, destination).toMutableList()
    init {
        connector = "path" {
            val (source, d1, d2, destination) = controlPoints
            "d"("M${source.x} ${source.y} C${d1.x} ${d1.y}, ${d2.x} ${d2.y}, ${destination.x} ${destination.y}")
            "fill"("none")
            "stroke-width"("2")
        }
        geometry {
            x(min(source.x, destination.x))
            y(min(source.y, destination.y))
            width(abs(source.x - destination.x))
            height(abs(source.y - destination.y))
        }
    }

    override fun applyArrowAtEnd(identifier: Int) {
        controlPoints[controlPoints.size - 1] =  replace(destination)

        val applyArrow : SvgElement.() -> Unit = {
            val (source, d1, d2, destination) = controlPoints
            "d"("M${source.x} ${source.y} C${d1.x} ${d1.y}, ${d2.x} ${d2.y}, ${destination.x} ${destination.y}")
            "marker-end"("url(#endarrow-$identifier)")
        }
        connector.applyArrow()
    }

    override fun applyArrowAtStart(identifier: Int) {
        controlPoints[0] =  replace(source)

        val applyArrow : SvgElement.() -> Unit = {
            val (source, d1, d2, destination) = controlPoints
            "d"("M${source.x} ${source.y} C${d1.x} ${d1.y}, ${d2.x} ${d2.y}, ${destination.x} ${destination.y}")
            "marker-start"("url(#startarrow-$identifier)")
        }
        connector.applyArrow()
    }

    companion object {
        fun createControlPoints(source: Anchor, destination: Anchor): List<Point> {
            val startHorizontally = (source.kind `is` (Anchor.Kind.Right or Anchor.Kind.Left))
            val endHorizontally = (destination.kind `is` (Anchor.Kind.Right or Anchor.Kind.Left))

            val (p1, p2) = Pair(source.point, destination.point)//.normalized()


            val c1 = if (startHorizontally) {
                Point(p2.x - 10, p1.y - .0)
            } else {
                Point(p1.x + .0, p2.y - 10)
            }

            val c2 = if (endHorizontally) {
                Point(p1.x + 10, p2.y + .0)
            } else {
                Point(p2.x + .0, p1.y - 10)
            }

            return listOf(source.point, c1, c2, destination.point)
        }
    }
}

private fun Pair<Point, Point>.normalized(): Pair<Point, Point> {
    val (p1, p2) = this
    val normP1 = Point(min(p1.x, p2.x), min(p1.y, p2.y))
    val normP2 = Point(max(p1.x, p2.x), max(p1.y, p2.y))

    return Pair(normP1, normP2)
}

fun replace(anchor: Anchor): Point {
    val (point, kind) = anchor
    return when (kind) {
        Anchor.Kind.Right -> point + Point(20.0, 0.0)
        Anchor.Kind.Left -> point + Point(-20.0, 0.0)
        Anchor.Kind.Bottom -> point + Point(0.0, 20.0)
        Anchor.Kind.Top -> point + Point(0.0, -20.0)
    }
}
