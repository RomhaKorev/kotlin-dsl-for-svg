package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.core.Attribute
import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.SvgElement
import ari.home.hexaofcontent.dsl.svg.SvgProperty

class Text(parent: SvgElement? = null): SvgElement("text", parent=parent) {

    private var x: Double? = null
    private var y: Double? = null
    fun x(value: Number) {
        x = value.toDouble()
    }

    fun y(value: Number) {
        y = value.toDouble()
    }

    override fun args(): String {
        val l = mutableListOf<SvgProperty>()
        l.add(SvgProperty("x", x ?: geometry().xOrNull()))
        l.add(SvgProperty("y", y ?: geometry().yOrNull()))
        return listOf(l.filter { it.valid() }.joinToString(" ") { it.render() }, super.args()).filter { it.isNotBlank() }.joinToString(" ")
    }
    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }
}

fun SvgElement.text(init: Text.() -> Unit): Text {
    val element = Text(this)
    element.init()
    this.children.add(element)
    return element
}
