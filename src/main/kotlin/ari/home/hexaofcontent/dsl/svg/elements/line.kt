package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.core.Attribute
import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.SvgElement
import kotlin.math.abs
import kotlin.math.min

class Line(parent: SvgElement? = null): SvgElement("line", parent=parent) {
    override fun Geometry.toAttributes(): List<Attribute> {
        return listOf()
    }

    var x1: Double? = null
    var y1: Double? = null
    var x2: Double? = null
    var y2: Double? = null

    fun x1(value: Number) {
        x1 = value.toDouble()
        "x1"(value)
    }

    fun x2(value: Number) {
        x2 = value.toDouble()
        "x2"(value)
    }

    fun y1(value: Number) {
        y1 = value.toDouble()
        "y1"(value)
    }

    fun y2(value: Number) {
        y2 = value.toDouble()
        "y2"(value)
    }

    internal fun defineGeometry() {
        if ((x1 == null) thenWarn "x1 is not defined!"
                ||
            (y1 == null) thenWarn "y1 is not defined!"
                ||
            (x2 == null) thenWarn "x2 is not defined!"
                ||
            (y2 == null) thenWarn "y2 is not defined!"
        )
            return

        geometry {
            x(min(x1!!, x2!!))
            y(min(y1!!, y2!!))
            width(abs(x1!! - x2!!))
            height(abs(y1!! - y2!!))
        }
    }
}

private fun create(init: Line.() -> Unit, parent: SvgElement?, generateGeometry: Boolean): Line {
    val line = Line(parent)
    line.init()
    if (generateGeometry && !line.geometry().valid())
        line.defineGeometry()
    return line
}

fun SvgElement.line(init: Line.() -> Unit): Line {
    return line(init, generateGeometry = true)
}

internal fun SvgElement.line(init: Line.() -> Unit, generateGeometry: Boolean): Line {
    val line = create(init, this, generateGeometry)
    this.children.add(line)
    return line
}
