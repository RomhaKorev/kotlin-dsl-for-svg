package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.core.Point

class SvgElementGroup(parent: SvgElement? = null): SvgElement("g", parent) {


    override fun geometry(): Geometry {
        return children.filterIsInstance<SvgElement>().fold(Geometry(0.0, 0.0, 0.0, 0.0, false)) {
                acc, v -> acc + v.geometry()
        } + translation
    }

    var translation =  Point(0.0, 0.0)

    override fun geometry(init: Geometry.() -> Unit) {
        val g = Geometry()
        g.init()
        translation = Point(g.xOrNull() ?: translation.x, g.yOrNull() ?: translation.y)
        "transform"("translate(${translation.x}, ${translation.y})")
    }
}


fun SvgElement.group(init: SvgElementGroup.() -> Unit): SvgElementGroup {
    val path = SvgElementGroup(this)
    this.children.add(path)
    path.init()
    return path
}
