package ari.home.hexaofcontent.dsl.svg.helpers.connectors

enum class Marker {
    None,
    Arrow,
}
