package ari.home.hexaofcontent.dsl.core

import ari.home.hexaofcontent.dsl.svg.SvgProperty


abstract class Renderer {
    private var zIndex: Int = 0
    private var visible: Boolean = true

    fun z(): Int {
        return zIndex
    }

    fun z(index: Int) {
        zIndex = index
    }

    abstract fun render(indent: Int=0): String
    open fun visible(): Boolean { return visible }
    fun hideIf(predicate: Boolean) {
        visible = !predicate
    }
}

open class Tag(val tag: String): Renderer() {
    val attributes = mutableListOf<Attribute>()
    val children = mutableListOf<Renderer>()


    open fun style(init: CssStyle.() -> Unit) {
        val style = CssStyle()
        style.init()
        attributes.add(style)
    }

    fun attribute(name: String, value: Any) {
        val attr = attributes.filterIsInstance<SvgProperty>().find { it.name == name }
        if (attr == null) {
            attributes.add(SvgProperty(name, value))
            return
        }
        attr(value)
    }

    fun attribute(name: String): Attribute {
        return attributes.filterIsInstance<SvgProperty>().find { it.name == name }
            ?: throw IllegalArgumentException("Argument $name does not exist")
    }

    private fun attributeExists(name: String): Boolean {
        return attributes.filterIsInstance<SvgProperty>().find { it.name == name } != null
    }

    operator fun String.invoke(value: Any) {
        attribute(this, value)
    }

    infix fun String.ifNotPresent(value: Any) {
        if (!attributeExists(this)) {
            attribute(this, value)
        }
    }

    protected open fun args(): String {
        return attributes.filter { it.valid() }.joinToString(" ") { it.render() }
    }

    protected fun Int.toTab(): String {
        return "    ".repeat(this)
    }

    protected fun String.toTag(args: String, indent: Int, content: () -> String): String {
        val inner = content()
        val inlineArgs = if (args.isBlank()) "" else " $args"
        if (inner == "") {
            return indent.toTab() + "<$this$inlineArgs />"
        }
        return indent.toTab() + "<$this$inlineArgs>\n${inner}\n" + indent.toTab() +  "</$this>"
    }


    override fun render(indent: Int): String {
        if (tag == "") {
            return this.children.sortedBy { it.z() }.filter { it.visible() }.joinToString("\n") { it.render(indent) }
        }
        return tag.toTag(args(), indent) {
            this.children.sortedBy { it.z() }.filter { it.visible() }.joinToString("\n") { it.render(indent + 1) }
        }
    }

    operator fun String.unaryMinus() {
        children.add(TextContent(this))
    }
}

fun Int.toTab(): String {
    return "    ".repeat(this)
}
