package ari.home.hexaofcontent.dsl.core

class CssStyle: Attribute("style") {
    val attributes = mutableListOf<CssProperty>()

    operator fun String.invoke(value: Any) {
        val attr = attributes.find { it.name == this }
        if (attr == null) {
            attributes.add(CssProperty(this, value))
            return
        }
        attr(value)
    }

    override fun render(): String {
        return "style=\"${value()}\""
    }

    override fun valid(): Boolean {
        return attributes.isNotEmpty()
    }

    override fun value(): String {
        return attributes.joinToString("; ") { it.render() }
    }

}
