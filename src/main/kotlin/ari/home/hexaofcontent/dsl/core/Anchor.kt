package ari.home.hexaofcontent.dsl.core

data class Anchor(val point: Point, val kind: Anchor.Kind) {
    val x: Double
        get() = point.x

    val y: Double
        get() = point.y

    enum class Kind {
        Top,
        Bottom,
        Left,
        Right;

        infix fun or(other: Anchor.Kind): List<Anchor.Kind> {
            return listOf(this, other)
        }

        infix fun `is`(values: List<Anchor.Kind>): Boolean {
            return values.contains(this)
        }
    }
}
