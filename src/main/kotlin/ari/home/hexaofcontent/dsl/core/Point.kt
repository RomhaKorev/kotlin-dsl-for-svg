package ari.home.hexaofcontent.dsl.core

import ari.home.hexaofcontent.dsl.svg.Geometry

import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

data class Point(val x: Double, val y: Double) {
    operator fun plus(other: Point): Point {
        return Point(x + other.x, y + other.y)
    }

    fun distance(other: Point): Double {
        return sqrt((this.x - other.x).pow(2.0) + (this.y - other.y).pow(2.0))
    }
}

fun List<Point>.boundingRect(): Geometry {
    val minX = this.map { it.x }.minOrNull() ?: 0.0
    val minY = this.map { it.y }.minOrNull() ?: 0.0
    val maxX = this.map { it.x }.maxOrNull() ?: 0.0
    val maxY = this.map { it.y }.maxOrNull() ?: 0.0

    return Geometry(minX, minY, abs(maxX - minX), abs(maxY - minY))
}
