package ari.home.hexaofcontent.dsl.core


data class Segment(val p1: Point, val p2: Point) {
    fun pointAt(ratio: Double): Point {
        val a = ratio
        val b = 1 - a
        val x = (b * p1.x + a * p2.x)
        val y = (b * p1.y + a * p2.y)
        return Point(x, y)
    }

    fun length(): Double {
        return Math.sqrt(Math.pow(p2.x - p1.x, 2.0) + Math.pow(p2.y - p1.y, 2.0))
    }

    fun pointAtPointFromEnd(distance: Double): Point {
        val ratio = (length() - distance) / length()
        return pointAt(ratio)
    }

    fun pointAtPointFromStart(distance: Double): Point {
        val ratio = distance / length()
        return pointAt(ratio)
    }
}
