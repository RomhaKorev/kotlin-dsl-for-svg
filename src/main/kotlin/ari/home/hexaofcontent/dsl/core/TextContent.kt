package ari.home.hexaofcontent.dsl.core

class TextContent(private val value: String): Renderer() {
    override fun render(indent: Int): String {
        return indent.toTab() + value
    }
}
