package ari.home.hexaofcontent.dsl.core

class CssProperty(val name: String, var value: Any) {
    fun render(): String {
        val valueString = value.toString().replace("\\.0$".toRegex(), "")
        return "$name: $valueString"
    }

    operator fun invoke(value: Any): CssProperty {
        this.value = value
        return this
    }
}
