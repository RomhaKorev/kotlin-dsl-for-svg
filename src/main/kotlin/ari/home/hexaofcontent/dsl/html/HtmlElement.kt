package ari.home.hexaofcontent.dsl.html

import ari.home.hexaofcontent.dsl.core.Tag

class HtmlElement(name: String): Tag(name) {
    operator fun String.invoke(init: HtmlElement.() -> Unit): HtmlElement {
        val instance = element(this, init)
        children.add(instance)
        return instance
    }

}
