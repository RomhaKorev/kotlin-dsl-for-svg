package ari.home.hexaofcontent.dsl.html

import ari.home.hexaofcontent.dsl.core.Attribute
import ari.home.hexaofcontent.dsl.core.Tag

class HtmlProperty(name: String, var value: Any): Attribute(name) {
    override fun render(): String {
        return "$name=\"${value()}\""
    }

    override fun value(): String {
        return value.toString().replace("\\.0$".toRegex(), "")
    }

    override fun valid(): Boolean {
        return value.toString() != ""
    }

    operator fun invoke(value: Any): HtmlProperty {
        this.value = value
        return this
    }
}

fun Tag.htmlProperty(name: String, value: String): HtmlProperty {
    val property = HtmlProperty(name, value)
    this.attributes.add(property)
    return property
}
