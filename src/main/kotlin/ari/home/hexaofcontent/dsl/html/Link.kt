package ari.home.hexaofcontent.dsl.html

import ari.home.hexaofcontent.dsl.core.Tag
import ari.home.hexaofcontent.dsl.core.toTab

class Link: Tag("link") {
    val rel = htmlProperty("rel", "")
    val href = htmlProperty("href", "")

    override fun render(indent: Int): String {
        return """${indent.toTab()}<link ${args()}>"""
    }
}


fun Tag.link(init: Link.() -> Unit) {
    val link = Link()
    link.init()
    this.children.add(link)
}
