package ari.home.hexaofcontent.dsl.html

import ari.home.hexaofcontent.dsl.core.Tag
import ari.home.hexaofcontent.dsl.core.toTab

class Meta: Tag("meta") {


    val name = htmlProperty("name", "")
    val content = htmlProperty("name", "")

    override fun render(indent: Int): String {
        return """${indent.toTab()}<meta ${args()}>"""
    }
}

fun Tag.meta(init: Meta.() -> Unit) {
    val meta = Meta()
    meta.init()
    this.children.add(meta)
}
