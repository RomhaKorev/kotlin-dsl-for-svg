package ari.home.hexaofcontent.dsl.html

fun orphan(tag: String, init: HtmlElement.() -> Unit): HtmlElement {
    return element(tag, init)
}

internal fun element(tag: String, init: HtmlElement.() -> Unit): HtmlElement {
    val instance = HtmlElement(tag)
    instance.init()
    return instance
}

fun html(init: HtmlElement.() -> Unit): HtmlElement {
    val element = orphan("html", init)
    return element
}
