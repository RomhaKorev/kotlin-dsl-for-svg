package ari.home.hexaofcontent.dsl.html

import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.invoke
import org.junit.jupiter.api.Test

class HtmlTest {

    @Test
    fun `Should Create a html content with head and body`() {
        html {
            "head" {
                meta {
                    name("viewport")
                    content("width=device-width")
                }
                link {
                    rel("stylesheet")
                    href("./css/foobar.css")
                }
            }
            "body" {
                "h1" {
                    - "Foobar"
                }
            }
        } `should give`
                """
                <html>
                    <head>
                        <meta name="viewport" name="width=device-width">
                        <link rel="stylesheet" href="./css/foobar.css">
                    </head>
                    <body>
                        <h1>
                            Foobar
                        </h1>
                    </body>
                </html>
                """.trimIndent()
    }
}
