package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.svg.elements.rect
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import javax.print.attribute.standard.JobOriginatingUserName

class ElementGroupTest {

    @Test
    fun `Should bound the content`() {
        svg {
            group {
                rect {
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
                rect {
                    geometry {
                        x(60)
                        y(20)
                        width(50)
                        height(50)
                    }
                }
            } `should be at` Geometry(10.0, 10.0, 100.0, 60.0)
        }
    }

    @Test
    fun `Should translate the content`() {
        svg {
            group {
                geometry {
                    x(30)
                    y(40)
                }
                rect {
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
                rect {
                    geometry {
                        x(60)
                        y(20)
                        width(50)
                        height(50)
                    }
                }
            }`should be at` Geometry(40.0, 50.0, 100.0, 60.0)  `should give`
                    """
                        <g transform="translate(30.0, 40.0)">
                            <rect x="10" y="10" width="50" height="50" />
                            <rect x="60" y="20" width="50" height="50" />
                        </g>
                    """.trimIndent()
        }
    }

    @Test
    fun `Should place the content below another`() {
        svg {
            group {
                id("group-1")
                rect {
                    id("r1")
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
            }
            group {
                geometry {
                    x(0)
                    10.below("group-1")
                }
                rect {
                    geometry {
                        x(10)
                        y(10)
                        width(50)
                        height(50)
                    }
                }
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="120" viewBox="10 10 50 120">
                    <g id="group-1">
                        <rect x="10" y="10" width="50" height="50" id="r1" />
                    </g>
                    <g transform="translate(0.0, 70.0)">
                        <rect x="10" y="10" width="50" height="50" />
                    </g>
                </svg>
                """.trimIndent()
    }


    @Test
    fun `Should align with another`() {
        svg {
            group {
                id("group-1")
                rect {
                    id("r1")
                    geometry {
                        x(20)
                        y(0)
                        width(50)
                        height(50)
                    }
                }
            }
            group {
                geometry {
                    y(50)
                }
                rect {
                    geometry {
                        x(0)
                        y(0)
                        width(10)
                        height(10)
                    }
                    "fill"("red")
                }
                alignRight("group-1")
            }
        } `should give`
                """
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="60" viewBox="20 0 50 60">
                    <g id="group-1">
                        <rect x="20" y="0" width="50" height="50" id="r1" />
                    </g>
                    <g transform="translate(60.0, 50.0)">
                        <rect x="0" y="0" width="10" height="10" fill="red" />
                    </g>
                </svg>
                """.trimIndent()
    }

}

infix fun SvgElement.`should be at`(expected: Geometry): SvgElement {
    val result = this.geometry()
    Assertions.assertEquals(expected, result, "Expecting ${result.print()} to be equals to ${expected.print()}")
    return this
}

fun Geometry.print(): String {
    return "(x=${xOrNull()}, y=${yOrNull()}, width=${widthOrNull()}, height=${heightOrNull()}, valid=${valid()})"
}
