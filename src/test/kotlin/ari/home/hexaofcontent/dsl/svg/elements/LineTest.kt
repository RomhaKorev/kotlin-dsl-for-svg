package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.orphan
import org.junit.jupiter.api.Test

class LineTest {

    @Test
    fun `should create a line`() {
        orphan {
            line {
                x1(10)
                y1(50)
                x2(25)
                y2(60)
            }
        }`should give` """<line x1="10" y1="50" x2="25" y2="60" />"""
    }

    @Test
    fun `should define the geometry of a line`() {
        orphan {
            line {
                x1(40)
                y1(50)
                x2(25)
                y2(60)
            }
        }.geometry() `should give` Geometry(25.0, 50.0, 40.0 - 25.0, 60.0 - 50.0)
    }
}
