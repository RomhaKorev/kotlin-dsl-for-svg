package ari.home.hexaofcontent.dsl.svg

import org.junit.jupiter.api.Test

class PolygonTest {

    @Test
    fun `Should create a polygon with points`() {
        "polygon" {
            "points"("75.0 0,150.0 37.5,150.0 112.5,75.0 150.0,0 112.5,0 37.5")
        } `should give`
                """
                <polygon points="75.0 0,150.0 37.5,150.0 112.5,75.0 150.0,0 112.5,0 37.5" />
                """.trimIndent()
    }
}
