package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.svg.`should contain`
import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.svg
import org.junit.jupiter.api.Test

class GTest {

    @Test
    fun `Should add stroke width parameter`() {
        svg {
            g {
                "stroke-width"("2px")
            }
        } `should contain`
        """<g stroke-width="2px" />""".trimIndent()
    }

    @Test
    fun `Should add stroke color parameter`() {
        svg {
        g {
            "stroke"("red")
        }
        } `should contain`
        """<g stroke="red" />"""
    }

    @Test
    fun `Should add transformation`() {
        svg {
        g {
            "transform"("translate(1 2)")
        }
        } `should contain`
                """<g transform="translate(1 2)" />"""
    }

    @Test
    fun `Should add fill color`() {
        svg{
            g {
            "fill"("red")
        }} `should contain`
                """<g fill="red" />"""
    }

    @Test
    fun `Should consider the size without adding properties`() {
        svg{
            g {
                geometry {
                    x(10)
                    y(20)
                    width(150)
                    height(300)
                }
                "fill"("red")
            }} `should contain`
                """<g fill="red" />"""
    }
}


