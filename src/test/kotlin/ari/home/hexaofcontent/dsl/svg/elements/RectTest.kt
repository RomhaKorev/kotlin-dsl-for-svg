package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.orphan
import org.junit.jupiter.api.Test

class RectTest {

    @Test
    fun `should create a rect`() {
        orphan {
            rect {
                x(10)
                y(50)
                width(25)
                height(60)
            }
        } `should give` """<rect x="10" y="50" width="25" height="60" />"""
    }
}
