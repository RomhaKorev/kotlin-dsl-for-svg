package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.core.Tag
import org.junit.jupiter.api.Assertions
import java.io.File


fun String.noindent(): String {
    return this.split("\n").joinToString("\n") { it.trim() }
}

infix fun Any.`should give`(expected: Any) {
    Assertions.assertEquals(expected, this)
}

infix fun Tag.`should give`(expected: String): Tag {
    Assertions.assertEquals(expected.noindent(), this.render().noindent())
    return this
}

infix fun Tag.`should contain`(expected: String): Tag {
    Assertions.assertEquals(expected, this.render().split("\n").drop(1).dropLast(1).joinToString("\n").trimIndent())
    return this
}

fun `should raise an error`(content: () -> Unit) {
    Assertions.assertThrows(IllegalArgumentException::class.java, content)
}

infix fun Tag.`save as`(fileName: String): Tag {
    val myfile = File(fileName)
    myfile.printWriter().use { out ->
        out.println(this.render())
    }
    return this
}
