package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.core.Point
import ari.home.hexaofcontent.dsl.core.boundingRect
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PointTest {

    @Test
    fun `Two points should be summed`() {
        val p1 = Point(10.0, 25.0)
        val p2 = Point(20.0, 30.0)

        Assertions.assertEquals(Point(30.0, 55.0), p1 + p2)
    }

    @Test
    fun `Summing a point with negative coords should be retrieved`() {
        val p1 = Point(10.0, 25.0)
        val p2 = Point(-40.0, -30.0)

        Assertions.assertEquals(Point(-30.0, -5.0), p1 + p2)
    }

    @Test
    fun `The bounding rect of a list of points should covered all points`() {
        val points = listOf<Point>(
                Point(10.0, 5.0),
                Point(40.0, 15.0),
                Point(60.0, -35.0),
                Point(10.0, 50.0),
                Point(-10.0, 25.0),
        )

        Assertions.assertEquals(Geometry(-10.0, -35.0, 70.0, 85.0), points.boundingRect())
    }


    @Test
    fun `Should give the distance between two points`() {
        val a = Point(10.0, 5.0)
        val b = Point(10.0, 105.0)

        Assertions.assertEquals(100.0, a.distance(b))
    }
}
