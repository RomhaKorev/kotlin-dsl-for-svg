package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.svg.elements.rect
import org.junit.jupiter.api.Test

class RectangleTest {
    @Test
    fun `Should create a polygon with points`() {
        orphan {
            rect {
                geometry {
                    x(12)
                    y(23)
                    width(120)
                    height(90)
                }
            } `should give`
                    """
                <rect x="12" y="23" width="120" height="90" />
                """.trimIndent()
        }
    }
}
