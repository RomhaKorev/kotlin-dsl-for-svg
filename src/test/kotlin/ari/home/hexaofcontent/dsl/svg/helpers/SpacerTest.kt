package ari.home.hexaofcontent.dsl.svg.helpers

import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.elements.g
import ari.home.hexaofcontent.dsl.svg.elements.rect
import ari.home.hexaofcontent.dsl.svg.svg
import org.junit.jupiter.api.Test

class SpacerTest {
    @Test
    fun `should create a spacer`() {
        svg {
            g {
                rect {
                    x(10)
                    y(20)
                    width(100)
                    height(120)
                }
            }
            spacer {
                x(110)
                y(15)
                width(10)
                height(150)
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="110" height="150" viewBox="10 15 110 150">
                <g>
                    <rect x="10" y="20" width="100" height="120" />
                </g>
            </svg>
        """.trimIndent()
    }
}
