package ari.home.hexaofcontent.dsl.svg

import org.junit.jupiter.api.Test

class UseTest {

    @Test
    fun `Should create a use with reference`() {
        "use" {
            "href"("hexagon")
        } `should give`
                """
                <use href="hexagon" />
                """.trimIndent()
    }
}
