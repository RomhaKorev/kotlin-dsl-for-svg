package ari.home.hexaofcontent.dsl.svg.helpers

import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.elements.circle
import ari.home.hexaofcontent.dsl.svg.helpers.connectors.ConnectorType
import ari.home.hexaofcontent.dsl.svg.helpers.connectors.connect
import ari.home.hexaofcontent.dsl.svg.svg
import org.junit.jupiter.api.Test

class CurvyConnectorTest: ConnectorTest(ConnectorType.Curvy) {

    override fun horizontalConnector(): String = """
                <path d="M120.0 45.0 C140.0 45.0, 130.0 45.0, 150.0 45" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
                """

    override fun verticalConnector(): String = """
                <path d="M70.0 70.0 C70.0 140.0, 70.0 60.0, 70.0 150" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
                """

    override fun diagonalConnector(): String = """
                <path d="M70.0 70.0 C70.0 140.0, 170.0 60.0, 170.0 150" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
                """

    override fun endingByAnArrow(): String = """
                <defs>
                    <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                        <polygon points="0 0, 10 3.5, 0 7" />
                    </marker>
                </defs>
                <path d="M70.0 70.0 C70.0 140.0, 170.0 60.0, 170.0 130" fill="none" stroke-width="2" marker-end="url(#endarrow-0)" style="stroke: black; stroke-width: 2" />
                """

    override fun startingAndEndingByAnArrow() = """
                <defs>
                    <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth">
                        <polygon points="0 0, 10 3.5, 0 7" />
                    </marker>
                    <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto">
                        <polygon points="10 0, 10 7, 0 3.5" />
                    </marker>
                </defs>
                <path d="M140.0 45.0 C170.0 45.0, 130.0 45.0, 160.0 45" fill="none" stroke-width="2" marker-end="url(#endarrow-0)" marker-start="url(#startarrow-0)" style="stroke: black; stroke-width: 2" />
                """


    override fun fromBottomToTop(): String = """
                <path d="M122.0 32.0 C122.0 82.0, 47.0 22.0, 47.0 92" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
            """

    override fun withLabel(): String = """
    <path d="M120.0 45.0 C240.0 45.0, 130.0 55.0, 250.0 55" fill="none" stroke-width="2" style="stroke: black; stroke-width: 2" />
    <text x="185" y="50" text-anchor="middle" dominant-baseline="middle">
        foobar
    </text>
    """

    override fun coloredConnector(): String = """
        <defs>
            <marker id="endarrow-0" markerWidth="10" markerHeight="7" refX="0" refY="3.5" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
                <polygon points="0 0, 10 3.5, 0 7" />
            </marker>
            <marker id="startarrow-0" markerWidth="10" markerHeight="7" refX="10" refY="3.5" orient="auto" stroke="red" fill="red">
                <polygon points="10 0, 10 7, 0 3.5" />
            </marker>
        </defs>
        <path d="M140.0 45.0 C240.0 45.0, 130.0 75.0, 230.0 75" fill="none" stroke-width="2" marker-end="url(#endarrow-0)" marker-start="url(#startarrow-0)" stroke="red" />
        <text x="185" y="60" text-anchor="middle" dominant-baseline="middle" fill="red">
            foobar
        </text>
        """

    override fun connectorStyle(): String = """
    <defs>
        <marker id="endarrow-0" markerWidth="5" markerHeight="3.5" refX="0" refY="1.75" orient="auto" markerUnits="strokeWidth" stroke="red" fill="red">
            <polygon points="0 0, 5 1.75, 0 3.5" />
        </marker>
    </defs>
    <path d="M20.0 40.0 C190.0 40.0, 30.0 100.0, 180.0 100" fill="none" stroke-width="3" marker-end="url(#endarrow-0)" stroke="red" />
    """.trimIndent()


    @Test
    fun `should draw a curvy connector from left bottom to right top`() {
        svg {
            //<circle id="origin-master-4" cx="145" cy="85" r="5" />
            circle {
                id("c1")
                cx(145)
                cy(85)
                r(5)
            }
            //<circle id="bob-feature-1-7" cx="205" cy="25" r="5" />
            circle {
                id("c2")
                cx(205)
                cy(25)
                r(5)
            }

            connect("c1".right(), "c2".left(), type = ConnectorType.Curvy) {
                color("black")
            }
        } `should give`  """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70" height="70" viewBox="140 20 70 70">
                <circle id="c1" cx="145" cy="85" r="5" />
                <circle id="c2" cx="205" cy="25" r="5" />
                <path d="M150.0 85.0 C190.0 85.0, 160.0 25.0, 200.0 25" fill="none" stroke-width="2" stroke="black" />
            </svg>
        """.trimIndent()
    }
}

