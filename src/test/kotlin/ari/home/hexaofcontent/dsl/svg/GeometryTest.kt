package ari.home.hexaofcontent.dsl.svg

import ari.home.hexaofcontent.dsl.svg.elements.rect
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GeometryTest {

    @Test
    fun `Should retrieve the geometry of a sibling`() {
        svg {
            rect {
                id("not source")
            }
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }
            rect {
                id("not source either")
            }

            Assertions.assertEquals(Geometry(120.0, 240.0, 60.0, 30.0, false), "source".geometry())
        }
    }

    @Test
    fun `Should apply the same size of a sibling`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                geometry {
                    x(0)
                    y(0)
                    sameSizeThan("source")
                }
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="180" height="270" viewBox="0 0 180 270">
                <rect x="120" y="240" width="60" height="30" id="source" />
                <rect x="0" y="0" width="60" height="30" />
            </svg>
        """.trimIndent()
    }

    @Test
    fun `Should ignore the geometry of a transparent element`() {
        svg {
            rect {
                id("source")
                geometry {
                    x(120)
                    y(240)
                    width(60)
                    height(30)
                }
            }

            rect {
                transparent()
                geometry {
                    x(200)
                    y(240)
                    width(80)
                    height(30)
                }
            }

            Assertions.assertEquals(Geometry(120.0, 240.0, 60.0, 30.0, false), "source".geometry())
        }
    }


    @Test
    fun `should sum two geometries`() {
        Geometry(10.0, 20.0, 100.0, 200.0) +  Geometry(-10.0, 30.0, 100.0, 200.0) `should give` Geometry(-10.0, 20.0, 120.0, 210.0)
    }

    @Test
    fun `left + invalid = left`() {
        Geometry(10.0, 20.0, 100.0, 200.0) +  Geometry() `should give` Geometry(10.0, 20.0, 100.0, 200.0)
    }

    @Test
    fun `invalid + right = right`() {
        Geometry() + Geometry(10.0, 20.0, 100.0, 200.0) `should give` Geometry(10.0, 20.0, 100.0, 200.0)
    }

    @Test
    fun `should reorder element by z index`() {
        svg {
            rect {
                x(100)
                y(200)
                width(100)
                height(100)
            }

            rect {
                z(-1)
                x(200)
                y(200)
                width(100)
                height(100)
            }

            rect {
                z(2)
                x(300)
                y(200)
                width(100)
                height(100)
            }
        } `should give` """
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="300" height="100" viewBox="100 200 300 100">
                <rect x="200" y="200" width="100" height="100" />
                <rect x="100" y="200" width="100" height="100" />
                <rect x="300" y="200" width="100" height="100" />
            </svg>
        """.trimIndent()
    }
}
