package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.core.Point
import ari.home.hexaofcontent.dsl.svg.Geometry
import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.orphan
import org.junit.jupiter.api.Test

class PolylineTest {
    @Test
    fun `should build a polyline`() {
        orphan {
            polyline {
                points(
                        Point(2.0, 3.0),
                        Point(15.0, 3.0),
                        Point(15.0, 20.0),
                        Point(2.0, 20.0),
                )
            }
        } `should give` """<polyline points="2 3, 15 3, 15 20, 2 20" />"""
    }

    @Test
    fun `should define the geometry according to the points`() {
        orphan {
            polyline {
                points(
                        Point(2.0, 3.0),
                        Point(15.0, 3.0),
                        Point(15.0, 20.0),
                        Point(2.0, 20.0),
                )
            }
        }.geometry() `should give` Geometry(2.0, 3.0, 13.0, 17.0)
    }
}
