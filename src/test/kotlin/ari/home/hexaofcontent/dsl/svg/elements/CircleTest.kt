package ari.home.hexaofcontent.dsl.svg.elements

import ari.home.hexaofcontent.dsl.svg.`should give`
import ari.home.hexaofcontent.dsl.svg.orphan
import org.junit.jupiter.api.Test

class CircleTest {

    @Test
    fun `should create a circle`() {
        orphan {
            circle {
                cx(10)
                cy(50)
                r(60)
            }
        } `should give` """<circle cx="10" cy="50" r="60" />"""
    }
}
